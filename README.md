# HTML2PDF Plugin for CakePHP #

## Setup ##

### Getting plugin ###

Clone source code from this repository into your `Plugin/HTML2PDF` directory.

### Load plugin ###

Load the HTML2PDF plugin in your bootstrap file, remember to use plugin bootstrap, like this :

    Cake::loadPlugin('HTML2PDF');

## Pdf ViewClass ##

### Allow PDF generation ###

This view allow to generate pdf files from HTML code, to activate this view, add this line in your controller :

    public $viewClass = 'HTML2PDF.Pdf';

## Branch strategy ##

This repository is splitted into following branches :

 * **master** : releases
 * **develop** : new features and bugs fix
